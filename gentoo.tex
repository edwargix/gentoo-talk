\documentclass{beamer}
\usepackage{nth}
\usepackage[utf8]{inputenc}
\usepackage{minted}


\title{Install Gentoo}
\author{David Florness}
\date{September \nth{12}, 2019}


\usetheme{Madrid}
\usecolortheme{whale}


\begin{document}
\begin{frame}
  \titlepage

  \begin{center}
    \includegraphics[width=0.25\textwidth]{img/larry.png}
  \end{center}
\end{frame}

\begin{frame}{An OS is composed of many parts\dots}
  \pause \dots and for each part, you have many choices \pause

  \begin{itemize}
  \item Kernel
    \begin{itemize}
    \item Linux
      \begin{itemize}
      \item stable
      \item hardened
      \end{itemize}
    \item *BSD kernels
    \end{itemize}
    \pause
  \item Init system
    \begin{itemize}
    \item systemd
    \item OpenRC
    \item runit
    \end{itemize}
    \pause
  \item Userland
    \begin{itemize}
    \item GNU
    \item Musl
    \item plan9
    \end{itemize}
  \end{itemize}
\end{frame}

\begin{frame}
  \begin{center}
    you distro makes these decisions for you\dots\pause \\
    \dots unless you use Gentoo
  \end{center}
\end{frame}

\begin{frame}{Decisions, decisions}
  With Gentoo, \textit{you} can decide

  \begin{itemize}
  \item which kernel you use \pause
  \item which init system you use \pause
  \item what specific features of a program want
  \end{itemize}
\end{frame}

\begin{frame}{How does Gentoo manage this?}
  \pause
  \begin{itemize}
  \item Gentoo is unique in that it is one of the few source-based distros
    \pause
  \item this means that every time you request a program be installed, the
    program's source code is downloaded from the program author's repo and
    compiled on your machine
  \end{itemize}
\end{frame}

\begin{frame}
  \begin{center}
    \includegraphics[width=\framewidth]{img/compile-it.png}
  \end{center}
\end{frame}

\begin{frame}{Kernel options}
  \pause
  \begin{itemize}
  \item Linux:
    \begin{itemize}
    \item gentoo-sources: standard, lightly patched for security updates
    \item ck-sources: Con Kolivas's kernel patch set that is primarily designed
      to improve system responsiveness and interactivity
    \item git-sources: tracks daily snapshots of the upstream
      (\href{https://kernel.org/}{kernel.org}) development kernel tree
    \end{itemize}
    \pause
  \item *BSD:
    \begin{itemize}
    \item Gentoo FreeBSD was a thing and theoretically still works, but it's
      been mostly abandoned
    \end{itemize}
  \end{itemize}
\end{frame}

\begin{frame}[fragile,allowframebreaks]{Init system options}
  \begin{itemize}
  \item systemd
  \item OpenRC (very similar to sysvinit, developed by Gentoo)
  \item runit
  \end{itemize}

  \framebreak

  \includegraphics[width=\framewidth]{img/init-systems.png}
\end{frame}

\begin{frame}{Program-level features}
  every program has a defined set of ``USE flags'' that toggle certain features
  of the program in question

  \includegraphics[width=\textwidth]{img/use-flags.png}
\end{frame}

\begin{frame}[fragile]{Configuring USE flags}
  \begin{block}{/etc/portage/package.use}
    \begin{minted}{ini}
      app-editors/emacs threads zlib acl

      sys-apps/util-linux static-libs

      mail-mta/postfix sasl dovecot-sasl
      mail-filter/spamassassin cron
      mail-filter/opendkim ldap sasl
    \end{minted}
  \end{block}
\end{frame}

\begin{frame}{USE flags in action}
  \begin{center}
    \includegraphics[width=\framewidth]{img/apache.png}
  \end{center}
\end{frame}

\begin{frame}{Flexibility over package versions}
  \begin{center}
    \includegraphics[width=0.7\textwidth]{img/versions.png}
  \end{center}
\end{frame}

\begin{frame}{What's the catch?}
  \begin{itemize}
  \item That everything on your Gentoo system needs to be compiled is a
    double-edged sword \pause
    \begin{itemize}
    \item On the one hand, this gives you utter control over everything \pause
    \item On the other hand, installing a program is no longer a simple
      matter. Much more thought and time~\footnote{upgrading your system can
        take hours to days, depending on your processor's performance} is
      required \pause
    \end{itemize}
  \item oh, and there's the whole installing thing\dots
  \end{itemize}
\end{frame}

\begin{frame}
  \begin{center}
    \includegraphics[width=0.65\framewidth]{img/1.png}
  \end{center}
\end{frame}
\begin{frame}
  \begin{center}
    \includegraphics[width=0.65\framewidth]{img/2.png}
  \end{center}
\end{frame}
\begin{frame}
  \begin{center}
    \includegraphics[width=0.65\framewidth]{img/3.png}
  \end{center}
\end{frame}
\begin{frame}
  \begin{center}
    \includegraphics[width=0.65\framewidth]{img/4.png}
  \end{center}
\end{frame}

\begin{frame}{Installing Gentoo}
  Steps from the Gentoo AMD64 handbook:
  \begin{enumerate}
  \item About the Gentoo Linux installation
  \item Choosing the right installation medium
  \item Configuring the network
  \item Preparing the disks
  \item Installing the Gentoo installation files
  \item Installing the Gentoo base system
  \item Configuring the Linux kernel
  \item Configuring the system
  \item Installing system tools
  \item Configuring the bootloader
  \item Finalizing the installation
  \end{enumerate}
\end{frame}

\begin{frame}{Compiling your Kernel}
  \pause This is without question the hardest part, and intimidates many,
  particularly because you need to decide \pause

  \begin{itemize}
  \item what bits of the kernel you need to utilize you your hardware \pause
  \item which parts should be builtin and which should be modules
  \end{itemize}
\end{frame}

\begin{frame}{Enabling Kernel features}
  \begin{center}
    \includegraphics[width=\textwidth]{img/menuconfig.png}
  \end{center}
\end{frame}

\begin{frame}{ebuilds and eclasses}
  \begin{itemize}
  \item used by portage to know \textit{how} to build packages
  \item reside in \texttt{/usr/portage/}
  \end{itemize}
\end{frame}
\begin{frame}[fragile]{ebuilds and eclasses}
  \begin{block}{/usr/portage/net-mail/mu/mu-1.0.ebuild}
    \inputminted[fontsize=\tiny]{bash}{mu.ebuild.1}
  \end{block}
\end{frame}
\begin{frame}[fragile]{ebuilds and eclasses}
  \begin{block}{/usr/portage/net-mail/mu/mu-1.0.ebuild}
    \inputminted[fontsize=\tiny]{bash}{mu.ebuild.2}
  \end{block}
\end{frame}
\begin{frame}[fragile]{ebuilds and eclasses}
  \begin{block}{/usr/portage/net-mail/mu/mu-1.0.ebuild}
    \inputminted[fontsize=\tiny]{bash}{mu.ebuild.3}
  \end{block}
\end{frame}

\begin{frame}{I want to show you\dots}
  \begin{itemize}
  \item the Gentoo wiki
  \item the PulseAudio wiki page
  \item the Gentoo handbook
  \item overlays
  \item Hardened Gentoo
  \end{itemize}
\end{frame}

\begin{frame}{Why tho?}
  \pause
  \begin{itemize}
  \item you learn a ton while installing and using gentoo \pause
  \item technically your programs run faster and occupy less space on your
    machine, but\dots \pause
  \item the real reason is it's \textit{FUN}
  \end{itemize}
\end{frame}

\end{document}
